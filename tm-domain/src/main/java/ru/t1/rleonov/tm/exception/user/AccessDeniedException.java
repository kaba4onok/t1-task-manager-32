package ru.t1.rleonov.tm.exception.user;

public final class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("Error! You are not logged in. Please log and try again...");
    }

}
