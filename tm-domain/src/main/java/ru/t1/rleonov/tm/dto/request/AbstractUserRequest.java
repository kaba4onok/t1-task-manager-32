package ru.t1.rleonov.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractUserRequest extends AbstractRequest {

    @Nullable
    private String userId;

}
