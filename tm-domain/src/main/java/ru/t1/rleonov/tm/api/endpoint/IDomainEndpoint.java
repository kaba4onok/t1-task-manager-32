package ru.t1.rleonov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.*;
import ru.t1.rleonov.tm.dto.response.*;

public interface IDomainEndpoint {

    @NotNull
    ServerLoadDataBackupResponse loadDataBackup(@NotNull ServerLoadDataBackupRequest request);

    @NotNull
    ServerLoadDataBase64Response loadDataBase64(@NotNull ServerLoadDataBase64Request request);

    @NotNull
    ServerLoadDataBinaryResponse loadDataBinary(@NotNull ServerLoadDataBinaryRequest request);

    @NotNull
    ServerLoadDataJsonFasterXmlResponse loadDataJsonFasterXml(@NotNull ServerLoadDataJsonFasterXmlRequest request);

    @NotNull
    ServerLoadDataJsonJaxBResponse loadDataJsonJaxB(@NotNull ServerLoadDataJsonJaxBRequest request);

    @NotNull
    ServerLoadDataXmlFasterXmlResponse loadDataXmlFasterXml(@NotNull ServerLoadDataXmlFasterXmlRequest request);

    @NotNull
    ServerLoadDataXmlJaxBResponse loadDataXmlJaxB(@NotNull ServerLoadDataXmlJaxBRequest request);

    @NotNull
    ServerLoadDataYamlFasterXmlResponse loadDataYamlFasterXml(@NotNull ServerLoadDataYamlFasterXmlRequest request);

    @NotNull
    ServerSaveDataBackupResponse saveDataBackup(@NotNull ServerSaveDataBackupRequest request);

    @NotNull
    ServerSaveDataBase64Response saveDataBase64(@NotNull ServerSaveDataBase64Request request);

    @NotNull
    ServerSaveDataBinaryResponse saveDataBinary(@NotNull ServerSaveDataBinaryRequest request);

    @NotNull
    ServerSaveDataJsonFasterXmlResponse saveDataJsonFasterXml(@NotNull ServerSaveDataJsonFasterXmlRequest request);

    @NotNull
    ServerSaveDataJsonJaxBResponse saveDataJsonJaxB(@NotNull ServerSaveDataJsonJaxBRequest request);

    @NotNull
    ServerSaveDataXmlFasterXmlResponse saveDataXmlFasterXml(@NotNull ServerSaveDataXmlFasterXmlRequest request);

    @NotNull
    ServerSaveDataXmlJaxBResponse saveDataXmlJaxB(@NotNull ServerSaveDataXmlJaxBRequest request);

    @NotNull
    ServerSaveDataYamlFasterXmlResponse saveDataYamlFasterXml(@NotNull ServerSaveDataYamlFasterXmlRequest request);

}
