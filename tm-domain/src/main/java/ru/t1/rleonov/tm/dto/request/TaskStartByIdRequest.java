package ru.t1.rleonov.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskStartByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private final Status status = Status.IN_PROGRESS;

}
