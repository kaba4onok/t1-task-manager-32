package ru.t1.rleonov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public final class UserChangePasswordProfileResponse extends AbstractUserResponse {

    public UserChangePasswordProfileResponse(@Nullable User user) {
        super(user);
    }

}
