package ru.t1.rleonov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.*;
import ru.t1.rleonov.tm.dto.response.*;

public interface IUserEndpoint {

    @NotNull
    UserChangePasswordProfileResponse changePassword(@NotNull UserChangePasswordProfileRequest request);

    @NotNull
    UserLockResponse lockUser(@NotNull UserLockRequest request);

    @NotNull
    UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

    @NotNull
    UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

    @NotNull
    UserRegistryResponse registryUser(@NotNull UserRegistryRequest request);

    @NotNull
    UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request);

    @NotNull
    UserViewProfileResponse viewUserProfile(@NotNull UserViewProfileRequest request);

}
