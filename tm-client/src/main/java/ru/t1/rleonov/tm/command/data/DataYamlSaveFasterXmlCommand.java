package ru.t1.rleonov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.ServerSaveDataYamlFasterXmlRequest;

public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-save-yaml-fasterxml";

    @NotNull
    private static final String DESCRIPTION = "Save data in yaml file using fasterxml.";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final ServerSaveDataYamlFasterXmlRequest request = new ServerSaveDataYamlFasterXmlRequest();
        getDomainEndpoint().saveDataYamlFasterXml(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
