package ru.t1.rleonov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.ServerLoadDataBase64Request;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";

    @NotNull
    private static final String DESCRIPTION = "Load data from base64 file.";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final ServerLoadDataBase64Request request = new ServerLoadDataBase64Request();
        getDomainEndpoint().loadDataBase64(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
