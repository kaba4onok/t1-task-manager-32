package ru.t1.rleonov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.ServerLoadDataYamlFasterXmlRequest;

public final class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-load-yaml";

    @NotNull
    private static final String DESCRIPTION = "Load data from yaml file.";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final ServerLoadDataYamlFasterXmlRequest request = new ServerLoadDataYamlFasterXmlRequest();
        getDomainEndpoint().loadDataYamlFasterXml(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
