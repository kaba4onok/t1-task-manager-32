package ru.t1.rleonov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.UserUpdateProfileRequest;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-update-profile";

    @NotNull
    private static final String DESCRIPTION = "Update profile of current user.";

    @Override
    public void execute() {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("ENTER LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest();
        request.setLastName(lastName);
        request.setFirstName(firstName);
        request.setMiddleName(middleName);
        getUserEndpoint().updateUserProfile(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
