package ru.t1.rleonov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.ProjectUpdateByIndexRequest;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String COMMAND = "project-update-by-index";

    @NotNull
    private static final String DESCRIPTION = "Update project by index.";

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest();
        request.setIndex(index);
        request.setName(name);
        request.setDescription(description);
        getProjectEndpoint().updateProjectByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return COMMAND;
    }

}
