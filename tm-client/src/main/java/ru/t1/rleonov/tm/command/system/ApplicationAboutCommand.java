package ru.t1.rleonov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.request.ServerAboutRequest;
import ru.t1.rleonov.tm.dto.response.ServerAboutResponse;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private static final  String ARGUMENT = "-a";

    @NotNull
    private static final  String NAME = "about";

    @NotNull
    private static final  String DESCRIPTION = "Show application info.";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @NotNull final ServerAboutRequest request = new ServerAboutRequest();
        @NotNull final ServerAboutResponse response = getSystemEndpoint().getAbout(request);
        System.out.println(response.getName());
        System.out.println(response.getEmail());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
