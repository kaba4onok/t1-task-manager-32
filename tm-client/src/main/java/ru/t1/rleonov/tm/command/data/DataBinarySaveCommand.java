package ru.t1.rleonov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.ServerSaveDataBinaryRequest;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-save-bin";

    @NotNull
    private static final String DESCRIPTION = "Save data to binary file.";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final ServerSaveDataBinaryRequest request = new ServerSaveDataBinaryRequest();
        getDomainEndpoint().saveDataBinary(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
