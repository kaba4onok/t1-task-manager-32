package ru.t1.rleonov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.ServerLoadDataJsonJaxBRequest;

public final class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-load-json-jaxb";

    @NotNull
    private static final String DESCRIPTION = "Load data from json file using jaxb.";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final ServerLoadDataJsonJaxBRequest request = new ServerLoadDataJsonJaxBRequest();
        getDomainEndpoint().loadDataJsonJaxB(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
