package ru.t1.rleonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.TaskStartByIdRequest;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String COMMAND = "task-start-by-id";

    @NotNull
    private static final String DESCRIPTION = "Start task by id.";

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest();
        request.setId(id);
        getTaskEndpoint().startTaskById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return COMMAND;
    }

}
