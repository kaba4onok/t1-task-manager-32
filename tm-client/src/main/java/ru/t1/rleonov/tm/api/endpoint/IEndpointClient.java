package ru.t1.rleonov.tm.api.endpoint;

import java.io.IOException;
import java.net.Socket;

public interface IEndpointClient {

    Socket connect() throws IOException;

    Socket disconnect() throws IOException;

}
