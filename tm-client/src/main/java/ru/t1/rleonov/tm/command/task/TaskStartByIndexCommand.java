package ru.t1.rleonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.TaskStartByIndexRequest;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String COMMAND = "task-start-by-index";

    @NotNull
    private static final String DESCRIPTION = "Start task by index.";

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest();
        request.setIndex(index);
        getTaskEndpoint().startTaskByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return COMMAND;
    }

}
