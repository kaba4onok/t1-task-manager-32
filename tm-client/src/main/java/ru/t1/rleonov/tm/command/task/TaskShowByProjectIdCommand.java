package ru.t1.rleonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.request.TaskShowByProjectIdRequest;
import ru.t1.rleonov.tm.model.Task;
import ru.t1.rleonov.tm.util.TerminalUtil;
import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String COMMAND = "task-show-by-project-id";

    @NotNull
    private static final String DESCRIPTION = "Show task list by project id.";

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskShowByProjectIdRequest request = new TaskShowByProjectIdRequest();
        request.setProjectId(projectId);
        @Nullable final List<Task> tasks = getTaskEndpoint().getTaskListByProjectId(request).getTasks();
        if (tasks == null) return;
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return COMMAND;
    }

}
