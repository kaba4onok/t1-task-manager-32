package ru.t1.rleonov.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.model.ICommand;
import ru.t1.rleonov.tm.api.service.IServiceLocator;
import ru.t1.rleonov.tm.enumerated.Role;

@Getter
@Setter
public abstract class AbstractCommand implements ICommand {

    @Nullable
    protected IServiceLocator serviceLocator;

    public abstract void execute();

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract String getName();

    public abstract Role[] getRoles();

    @NotNull
    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
