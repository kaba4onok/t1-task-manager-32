package ru.t1.rleonov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.endpoint.IAuthEndpointClient;
import ru.t1.rleonov.tm.dto.request.UserLoginRequest;
import ru.t1.rleonov.tm.dto.request.UserLogoutRequest;
import ru.t1.rleonov.tm.dto.request.UserProfileRequest;
import ru.t1.rleonov.tm.dto.response.UserLoginResponse;
import ru.t1.rleonov.tm.dto.response.UserLogoutResponse;
import ru.t1.rleonov.tm.dto.response.UserProfileResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLoginResponse login(@NotNull UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserProfileResponse profile(@NotNull UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
        System.out.println(authEndpointClient.login(new UserLoginRequest("user11", "user11")).getSuccess());
        System.out.println(authEndpointClient.login(new UserLoginRequest("user2", "user2")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser().getEmail());
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
        authEndpointClient.disconnect();
    }

}
