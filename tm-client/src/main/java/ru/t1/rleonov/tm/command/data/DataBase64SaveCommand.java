package ru.t1.rleonov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.ServerSaveDataBase64Request;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-save-base64";

    @NotNull
    private static final String DESCRIPTION = "Save base 64 data in file.";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final ServerSaveDataBase64Request request = new ServerSaveDataBase64Request();
        getDomainEndpoint().saveDataBase64(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
