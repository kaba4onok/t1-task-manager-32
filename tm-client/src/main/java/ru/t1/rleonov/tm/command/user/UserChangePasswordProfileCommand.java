package ru.t1.rleonov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.UserChangePasswordProfileRequest;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class UserChangePasswordProfileCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-update-profile";

    @NotNull
    private static final String DESCRIPTION = "Update profile of current user.";

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordProfileRequest request = new UserChangePasswordProfileRequest();
        request.setPassword(password);
        getUserEndpoint().changePassword(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
