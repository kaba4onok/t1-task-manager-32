package ru.t1.rleonov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.endpoint.IDomainEndpointClient;
import ru.t1.rleonov.tm.dto.request.*;
import ru.t1.rleonov.tm.dto.response.*;

@NoArgsConstructor
public final class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpointClient {

    public DomainEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerLoadDataBackupResponse loadDataBackup(@NotNull ServerLoadDataBackupRequest request) {
        return call(request, ServerLoadDataBackupResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerLoadDataBase64Response loadDataBase64(@NotNull ServerLoadDataBase64Request request) {
        return call(request, ServerLoadDataBase64Response.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerLoadDataBinaryResponse loadDataBinary(@NotNull ServerLoadDataBinaryRequest request) {
        return call(request, ServerLoadDataBinaryResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerLoadDataJsonFasterXmlResponse loadDataJsonFasterXml(@NotNull ServerLoadDataJsonFasterXmlRequest request) {
        return call(request, ServerLoadDataJsonFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerLoadDataJsonJaxBResponse loadDataJsonJaxB(@NotNull ServerLoadDataJsonJaxBRequest request) {
        return call(request, ServerLoadDataJsonJaxBResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerLoadDataXmlFasterXmlResponse loadDataXmlFasterXml(@NotNull ServerLoadDataXmlFasterXmlRequest request) {
        return call(request, ServerLoadDataXmlFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerLoadDataXmlJaxBResponse loadDataXmlJaxB(@NotNull ServerLoadDataXmlJaxBRequest request) {
        return call(request, ServerLoadDataXmlJaxBResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerLoadDataYamlFasterXmlResponse loadDataYamlFasterXml(@NotNull ServerLoadDataYamlFasterXmlRequest request) {
        return call(request, ServerLoadDataYamlFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerSaveDataBackupResponse saveDataBackup(@NotNull ServerSaveDataBackupRequest request) {
        return call(request, ServerSaveDataBackupResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerSaveDataBase64Response saveDataBase64(@NotNull ServerSaveDataBase64Request request) {
        return call(request, ServerSaveDataBase64Response.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerSaveDataBinaryResponse saveDataBinary(@NotNull ServerSaveDataBinaryRequest request) {
        return call(request, ServerSaveDataBinaryResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerSaveDataJsonFasterXmlResponse saveDataJsonFasterXml(@NotNull ServerSaveDataJsonFasterXmlRequest request) {
        return call(request, ServerSaveDataJsonFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerSaveDataJsonJaxBResponse saveDataJsonJaxB(@NotNull ServerSaveDataJsonJaxBRequest request) {
        return call(request, ServerSaveDataJsonJaxBResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerSaveDataXmlFasterXmlResponse saveDataXmlFasterXml(@NotNull ServerSaveDataXmlFasterXmlRequest request) {
        return call(request, ServerSaveDataXmlFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerSaveDataXmlJaxBResponse saveDataXmlJaxB(@NotNull ServerSaveDataXmlJaxBRequest request) {
        return call(request, ServerSaveDataXmlJaxBResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerSaveDataYamlFasterXmlResponse saveDataYamlFasterXml(@NotNull ServerSaveDataYamlFasterXmlRequest request) {
        return call(request, ServerSaveDataYamlFasterXmlResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        {
            System.out.println(authEndpointClient.login(new UserLoginRequest("admin", "admin")));
            @NotNull final DomainEndpointClient domainEndpointClient = new DomainEndpointClient(authEndpointClient);
            domainEndpointClient.saveDataBase64(new ServerSaveDataBase64Request());
        }
        {
            System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")));
            @NotNull final DomainEndpointClient domainEndpointClient = new DomainEndpointClient(authEndpointClient);
            domainEndpointClient.saveDataBase64(new ServerSaveDataBase64Request());
        }
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }

}
