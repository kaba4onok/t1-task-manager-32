package ru.t1.rleonov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.endpoint.IAuthEndpointClient;
import ru.t1.rleonov.tm.api.endpoint.IUserEndpointClient;
import ru.t1.rleonov.tm.command.AbstractCommand;
import ru.t1.rleonov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserEndpointClient getUserEndpoint() {
        return getServiceLocator().getUserEndpointClient();
    }

    @NotNull
    protected IAuthEndpointClient getAuthEndpoint() {
        return getServiceLocator().getAuthEndpointClient();
    }

    protected void showUser(@Nullable final User user) {
        if (user == null) return;
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
