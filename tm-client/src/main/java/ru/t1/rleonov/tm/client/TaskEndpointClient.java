package ru.t1.rleonov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.endpoint.ITaskEndpointClient;
import ru.t1.rleonov.tm.dto.request.*;
import ru.t1.rleonov.tm.dto.response.*;

@NoArgsConstructor
public final class TaskEndpointClient extends AbstractEndpointClient implements ITaskEndpointClient {

    public TaskEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUnbindFromProjectResponse unbindTaskToProject(@NotNull TaskUnbindFromProjectRequest request) {
        return call(request, TaskUnbindFromProjectResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request) {
        return call(request, TaskChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskClearResponse clearTask(@NotNull TaskClearRequest request) {
        return call(request, TaskClearResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCompleteByIdResponse completeTaskById(@NotNull TaskCompleteByIdRequest request) {
        return call(request, TaskCompleteByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCompleteByIndexResponse completeTaskByIndex(@NotNull TaskCompleteByIndexRequest request) {
        return call(request, TaskCompleteByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCreateResponse createTask(@NotNull TaskCreateRequest request) {
        return call(request, TaskCreateResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskListResponse getTaskList(@NotNull TaskListRequest request) {
        return call(request, TaskListResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRemoveByIndexResponse removeTaskByIndex(@NotNull TaskRemoveByIndexRequest request) {
        return call(request, TaskRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskShowByIdResponse showTaskById(@NotNull TaskShowByIdRequest request) {
        return call(request, TaskShowByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskShowByIndexResponse showTaskByIndex(@NotNull TaskShowByIndexRequest request) {
        return call(request, TaskShowByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskShowByProjectIdResponse getTaskListByProjectId(@NotNull TaskShowByProjectIdRequest request) {
        return call(request, TaskShowByProjectIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskStartByIdResponse startTaskById(@NotNull TaskStartByIdRequest request) {
        return call(request, TaskStartByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskStartByIndexResponse startTaskByIndex(@NotNull TaskStartByIndexRequest request) {
        return call(request, TaskStartByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUpdateByIndexResponse updateTaskByIndex(@NotNull TaskUpdateByIndexRequest request) {
        return call(request, TaskUpdateByIndexResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("user1", "user1")));
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser().getEmail());

        @NotNull final TaskEndpointClient taskEndpointClient = new TaskEndpointClient(authEndpointClient);
        System.out.println(taskEndpointClient.createTask(new TaskCreateRequest("TAAASK", "DESCRIIIIPTION")));
        System.out.println(taskEndpointClient.getTaskList(new TaskListRequest()).getTasks());

        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }

}
