package ru.t1.rleonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.request.TaskShowByIndexRequest;
import ru.t1.rleonov.tm.exception.entity.TaskNotFoundException;
import ru.t1.rleonov.tm.model.Task;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String COMMAND = "task-show-by-index";

    @NotNull
    private static final String DESCRIPTION = "Show task by index.";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest();
        request.setIndex(index);
        @Nullable final Task task = getTaskEndpoint().showTaskByIndex(request).getTask();
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return COMMAND;
    }

}
