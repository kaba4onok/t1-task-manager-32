package ru.t1.rleonov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.ServerLoadDataXmlFasterXmlRequest;

public final class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-load-xml-fasterxml";

    @NotNull
    private static final String DESCRIPTION = "Load data from xml file using fasterxml.";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final ServerLoadDataXmlFasterXmlRequest request = new ServerLoadDataXmlFasterXmlRequest();
        getDomainEndpoint().loadDataXmlFasterXml(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
