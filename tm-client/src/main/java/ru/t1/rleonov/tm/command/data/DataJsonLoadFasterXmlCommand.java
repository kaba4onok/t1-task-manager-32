package ru.t1.rleonov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.ServerLoadDataJsonFasterXmlRequest;

public final class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-load-json-fasterxml";

    @NotNull
    private static final String DESCRIPTION = "Load data from json file using fasterxml.";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final ServerLoadDataJsonFasterXmlRequest request = new ServerLoadDataJsonFasterXmlRequest();
        getDomainEndpoint().loadDataJsonFasterXml(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
