package ru.t1.rleonov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.request.UserViewProfileRequest;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-view-profile";

    @NotNull
    private static final String DESCRIPTION = "View profile of current user.";

    @Override
    public void execute() {
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest();
        @Nullable final User user = getUserEndpoint().viewUserProfile(request).getUser();
        if (user == null) return;
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
