package ru.t1.rleonov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.request.UserLoginRequest;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "login";

    @NotNull
    private static final String DESCRIPTION = "User login.";

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        getAuthEndpoint().login(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
