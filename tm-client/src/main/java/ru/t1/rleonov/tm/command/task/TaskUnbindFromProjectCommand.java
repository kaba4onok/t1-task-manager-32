package ru.t1.rleonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    private static final String COMMAND = "task-unbind-from-project";

    @NotNull
    private static final String DESCRIPTION = "Unbind task from project.";

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest();
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        getTaskEndpoint().unbindTaskToProject(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return COMMAND;
    }

}
