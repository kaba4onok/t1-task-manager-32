package ru.t1.rleonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.TaskRemoveByIndexRequest;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class TaskRemoveByIndex extends AbstractTaskCommand {

    @NotNull
    private static final String COMMAND = "task-remove-by-index";

    @NotNull
    private static final String DESCRIPTION = "Remove task by index.";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest();
        request.setIndex(index);
        getTaskEndpoint().removeTaskByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return COMMAND;
    }

}
