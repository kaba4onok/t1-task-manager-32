package ru.t1.rleonov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public interface IAuthEndpointClient extends IAuthEndpoint {

    void setSocket(@Nullable Socket socket);

}
