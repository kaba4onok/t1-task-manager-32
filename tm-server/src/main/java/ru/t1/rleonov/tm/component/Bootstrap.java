package ru.t1.rleonov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.endpoint.*;
import ru.t1.rleonov.tm.api.repository.IProjectRepository;
import ru.t1.rleonov.tm.api.repository.ITaskRepository;
import ru.t1.rleonov.tm.api.repository.IUserRepository;
import ru.t1.rleonov.tm.api.service.*;
import ru.t1.rleonov.tm.dto.request.*;
import ru.t1.rleonov.tm.endpoint.*;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.model.Project;
import ru.t1.rleonov.tm.model.Task;
import ru.t1.rleonov.tm.model.User;
import ru.t1.rleonov.tm.repository.ProjectRepository;
import ru.t1.rleonov.tm.repository.TaskRepository;
import ru.t1.rleonov.tm.repository.UserRepository;
import ru.t1.rleonov.tm.service.*;
import ru.t1.rleonov.tm.util.SystemUtil;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeProjectStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeProjectStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clearProject);
        server.registry(ProjectCompleteByIdRequest.class, projectEndpoint::completeProjectById);
        server.registry(ProjectCompleteByIndexRequest.class, projectEndpoint::completeProjectByIndex);
        server.registry(ProjectCreateRequest.class, projectEndpoint::createProject);
        server.registry(ProjectListRequest.class, projectEndpoint::getProjectList);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeProjectById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeProjectByIndex);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::showProjectById);
        server.registry(ProjectShowByIndexRequest.class, projectEndpoint::showProjectByIndex);
        server.registry(ProjectStartByIdRequest.class, projectEndpoint::startProjectById);
        server.registry(ProjectStartByIndexRequest.class, projectEndpoint::startProjectByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateProjectById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateProjectByIndex);

        server.registry(ServerLoadDataBackupRequest.class, domainEndpoint::loadDataBackup);
        server.registry(ServerLoadDataBase64Request.class, domainEndpoint::loadDataBase64);
        server.registry(ServerLoadDataBinaryRequest.class, domainEndpoint::loadDataBinary);
        server.registry(ServerLoadDataJsonFasterXmlRequest.class, domainEndpoint::loadDataJsonFasterXml);
        server.registry(ServerLoadDataJsonJaxBRequest.class, domainEndpoint::loadDataJsonJaxB);
        server.registry(ServerLoadDataXmlFasterXmlRequest.class, domainEndpoint::loadDataXmlFasterXml);
        server.registry(ServerLoadDataXmlJaxBRequest.class, domainEndpoint::loadDataXmlJaxB);
        server.registry(ServerLoadDataYamlFasterXmlRequest.class, domainEndpoint::loadDataYamlFasterXml);
        server.registry(ServerSaveDataBackupRequest.class, domainEndpoint::saveDataBackup);
        server.registry(ServerSaveDataBase64Request.class, domainEndpoint::saveDataBase64);
        server.registry(ServerSaveDataBinaryRequest.class, domainEndpoint::saveDataBinary);
        server.registry(ServerSaveDataJsonFasterXmlRequest.class, domainEndpoint::saveDataJsonFasterXml);
        server.registry(ServerSaveDataJsonJaxBRequest.class, domainEndpoint::saveDataJsonJaxB);
        server.registry(ServerSaveDataXmlFasterXmlRequest.class, domainEndpoint::saveDataXmlFasterXml);
        server.registry(ServerSaveDataXmlJaxBRequest.class, domainEndpoint::saveDataXmlJaxB);
        server.registry(ServerSaveDataYamlFasterXmlRequest.class, domainEndpoint::saveDataYamlFasterXml);

        server.registry(TaskBindToProjectRequest.class, taskEndpoint::bindTaskToProject);
        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeTaskStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeTaskStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clearTask);
        server.registry(TaskCompleteByIdRequest.class, taskEndpoint::completeTaskById);
        server.registry(TaskCompleteByIndexRequest.class, taskEndpoint::completeTaskByIndex);
        server.registry(TaskCreateRequest.class, taskEndpoint::createTask);
        server.registry(TaskListRequest.class, taskEndpoint::getTaskList);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeTaskById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeTaskByIndex);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::showTaskById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::showTaskByIndex);
        server.registry(TaskShowByProjectIdRequest.class, taskEndpoint::getTaskListByProjectId);
        server.registry(TaskStartByIdRequest.class, taskEndpoint::startTaskById);
        server.registry(TaskStartByIndexRequest.class, taskEndpoint::startTaskByIndex);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindTaskToProject);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateTaskById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateTaskByIndex);

        server.registry(UserChangePasswordProfileRequest.class, userEndpoint::changePassword);
        server.registry(UserLockRequest.class, userEndpoint::lockUser);
        server.registry(UserRegistryRequest.class, userEndpoint::registryUser);
        server.registry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockUser);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateUserProfile);
        server.registry(UserViewProfileRequest.class, userEndpoint::viewUserProfile);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        @NotNull final User user1 = userService.create("user1", "user1", "user1@mail.ru");
        @NotNull final User user2 = userService.create("user2", "user2", "user2@mail.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(user1.getId(), new Project("FIRST PROJECT", Status.COMPLETED));
        projectService.add(user1.getId(), new Project("A SECOND PROJECT", Status.IN_PROGRESS));
        projectService.add(user2.getId(), new Project("THIRD PROJECT", Status.NOT_STARTED));

        taskService.add(user1.getId(), new Task("FIRST TASK", Status.COMPLETED));
        taskService.add(user1.getId(), new Task("A SECOND TASK", Status.IN_PROGRESS));
        taskService.add(user2.getId(), new Task("THIRD TASK", Status.NOT_STARTED));
    }

    public void run() {
        initPID();
        initDemoData();
        loggerService.info("***WELCOME TO TASK-MANAGER***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        server.start();
    }

    private void prepareShutdown() {
        loggerService.info("***TASK-MANAGER IS SHUTTING DOWN***");
        backup.stop();
        server.stop();
    }

}
