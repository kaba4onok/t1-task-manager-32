package ru.t1.rleonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.rleonov.tm.api.service.IDomainService;
import ru.t1.rleonov.tm.api.service.IServiceLocator;
import ru.t1.rleonov.tm.dto.request.*;
import ru.t1.rleonov.tm.dto.response.*;
import ru.t1.rleonov.tm.enumerated.Role;

public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IDomainService getDomainService() {
        return getServiceLocator().getDomainService();
    }

    @Override
    @NotNull
    public ServerLoadDataBackupResponse loadDataBackup(@NotNull final ServerLoadDataBackupRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBackup();
        return new ServerLoadDataBackupResponse();
    }

    @Override
    @NotNull
    public ServerLoadDataBase64Response loadDataBase64(@NotNull final ServerLoadDataBase64Request request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBase64();
        return new ServerLoadDataBase64Response();
    }

    @Override
    @NotNull
    public ServerLoadDataBinaryResponse loadDataBinary(@NotNull final ServerLoadDataBinaryRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBinary();
        return new ServerLoadDataBinaryResponse();
    }

    @Override
    @NotNull
    public ServerLoadDataJsonFasterXmlResponse loadDataJsonFasterXml(@NotNull final ServerLoadDataJsonFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataJsonFasterXml();
        return new ServerLoadDataJsonFasterXmlResponse();
    }

    @Override
    @NotNull
    public ServerLoadDataJsonJaxBResponse loadDataJsonJaxB(@NotNull final ServerLoadDataJsonJaxBRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataJsonJaxB();
        return new ServerLoadDataJsonJaxBResponse();
    }

    @Override
    @NotNull
    public ServerLoadDataXmlFasterXmlResponse loadDataXmlFasterXml(@NotNull final ServerLoadDataXmlFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataXmlFasterXml();
        return new ServerLoadDataXmlFasterXmlResponse();
    }

    @Override
    @NotNull
    public ServerLoadDataXmlJaxBResponse loadDataXmlJaxB(@NotNull final ServerLoadDataXmlJaxBRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataXmlJaxB();
        return new ServerLoadDataXmlJaxBResponse();
    }

    @Override
    @NotNull
    public ServerLoadDataYamlFasterXmlResponse loadDataYamlFasterXml(@NotNull final ServerLoadDataYamlFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataYamlFasterXml();
        return new ServerLoadDataYamlFasterXmlResponse();
    }

    @Override
    @NotNull
    public ServerSaveDataBackupResponse saveDataBackup(@NotNull final ServerSaveDataBackupRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBackup();
        return new ServerSaveDataBackupResponse();
    }

    @Override
    @NotNull
    public ServerSaveDataBase64Response saveDataBase64(@NotNull final ServerSaveDataBase64Request request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBase64();
        return new ServerSaveDataBase64Response();
    }

    @Override
    @NotNull
    public ServerSaveDataBinaryResponse saveDataBinary(@NotNull final ServerSaveDataBinaryRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBinary();
        return new ServerSaveDataBinaryResponse();
    }

    @Override
    @NotNull
    public ServerSaveDataJsonFasterXmlResponse saveDataJsonFasterXml(@NotNull final ServerSaveDataJsonFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataJsonFasterXml();
        return new ServerSaveDataJsonFasterXmlResponse();
    }

    @Override
    @NotNull
    public ServerSaveDataJsonJaxBResponse saveDataJsonJaxB(@NotNull final ServerSaveDataJsonJaxBRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataJsonJaxB();
        return new ServerSaveDataJsonJaxBResponse();
    }

    @Override
    @NotNull
    public ServerSaveDataXmlFasterXmlResponse saveDataXmlFasterXml(@NotNull final ServerSaveDataXmlFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataXmlFasterXml();
        return new ServerSaveDataXmlFasterXmlResponse();
    }

    @Override
    @NotNull
    public ServerSaveDataXmlJaxBResponse saveDataXmlJaxB(@NotNull final ServerSaveDataXmlJaxBRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataXmlJaxB();
        return new ServerSaveDataXmlJaxBResponse();
    }

    @Override
    @NotNull
    public ServerSaveDataYamlFasterXmlResponse saveDataYamlFasterXml(@NotNull final ServerSaveDataYamlFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataYamlFasterXml();
        return new ServerSaveDataYamlFasterXmlResponse();
    }

}
