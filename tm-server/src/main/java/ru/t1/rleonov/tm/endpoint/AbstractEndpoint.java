package ru.t1.rleonov.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.service.IServiceLocator;
import ru.t1.rleonov.tm.api.service.IUserService;
import ru.t1.rleonov.tm.dto.request.AbstractUserRequest;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.exception.user.AccessDeniedException;
import ru.t1.rleonov.tm.model.User;

public abstract class AbstractEndpoint {

    protected void check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String userid = request.getUserId();
        if (userid == null || userid.isEmpty()) throw new AccessDeniedException();
        @NotNull final IServiceLocator serviceLocator = getServiceLocator();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findOneById(userid);
        if (user == null) throw new AccessDeniedException();
        @Nullable final Role roleUser = user.getRole();
        final boolean check = roleUser == role;
        if (!check) throw new AccessDeniedException();
    }

    protected void check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String userid = request.getUserId();
        if (userid == null || userid.isEmpty()) throw new AccessDeniedException();
    }

    @Getter
    @NotNull
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
