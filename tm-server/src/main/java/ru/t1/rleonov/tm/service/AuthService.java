package ru.t1.rleonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.service.IAuthService;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.api.service.IUserService;
import ru.t1.rleonov.tm.exception.field.EmailEmptyException;
import ru.t1.rleonov.tm.exception.field.LoginEmptyException;
import ru.t1.rleonov.tm.exception.field.PasswordEmptyException;
import ru.t1.rleonov.tm.exception.user.LoginOrPasswordIncorrectException;
import ru.t1.rleonov.tm.model.User;
import ru.t1.rleonov.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    public AuthService(
            @NotNull IPropertyService propertyService,
            @NotNull IUserService userService
    ) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @NotNull
    @Override
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return userService.create(login, password, email);
    }

    @Override
    public User check(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new LoginOrPasswordIncorrectException();
        if (user.getLocked()) throw new LoginOrPasswordIncorrectException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new LoginOrPasswordIncorrectException();
        if (!hash.equals(user.getPasswordHash())) throw new LoginOrPasswordIncorrectException();
        return user;
    }

}
